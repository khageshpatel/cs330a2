// stats.h 
//	Routines for managing statistics about Nachos performance.
//
// DO NOT CHANGE -- these stats are maintained by the machine emulation.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "utility.h"
#include "stats.h"
#include "system.h"
//----------------------------------------------------------------------
// Statistics::Statistics
// 	Initialize performance metrics to zero, at system startup.
//----------------------------------------------------------------------

Statistics::Statistics()
{
    totalTicks = idleTicks = systemTicks = userTicks = 0;
    numDiskReads = numDiskWrites = 0;
    numConsoleCharsRead = numConsoleCharsWritten = 0;
    numPageFaults = numPacketsSent = numPacketsRecvd = 0;
    maxCPUBurst = countCPUBurst = totalCPUBurst = 0;
    minCPUBurst = minCompletionTime = 0x7FFFFFFF;
    totalWaitTime = numThreads = 0;
    noOfCompletedThreads = totalCompletionTime = totalSqCompletionTime = 0;
    maxCompletionTime = totalBurstError = 0;
}

//Update CPU burst statistics
void Statistics::newCPUBurst(int Burst){

	if(Burst == 0)     //We dont store statistics of 0 length bursts
		return;
	countCPUBurst++;	
	maxCPUBurst = (Burst>maxCPUBurst)?Burst:maxCPUBurst;
	minCPUBurst = (Burst<minCPUBurst)?Burst:minCPUBurst;
	totalCPUBurst += Burst;

}

//Called when a new thread is completed
void Statistics::newCompletedThread(int t){
	noOfCompletedThreads++;
	totalCompletionTime += t;
	totalSqCompletionTime += ((long long)t*t);
	maxCompletionTime = (t>maxCompletionTime)?t:maxCompletionTime;
	minCompletionTime = (t<minCompletionTime)?t:minCompletionTime;
}

//----------------------------------------------------------------------
// Statistics::Print
// 	Print performance metrics, when we've finished everything
//	at system shutdown.
//----------------------------------------------------------------------

void
Statistics::Print()
{
    printf("Ticks: total %d, idle %d, system %d, user %d\n", totalTicks, 
	idleTicks, systemTicks, userTicks);
    printf("Disk I/O: reads %d, writes %d\n", numDiskReads, numDiskWrites);
    printf("Console I/O: reads %d, writes %d\n", numConsoleCharsRead, 
	numConsoleCharsWritten);
    printf("Paging: faults %d\n", numPageFaults);
    printf("Network I/O: packets received %d, sent %d\n", numPacketsRecvd, 
	numPacketsSent);

    //Assignment stats goes here
    printf("Total CPU busy time:%d\n",systemTicks+userTicks);
    printf("Total Execution time:%d\n",totalTicks);
    printf("CPU Utilization:%lf\n",((systemTicks+userTicks)*100.0)/totalTicks);

    printf("Maximum observed CPU burst: %d\n",maxCPUBurst);
    printf("Minimum observer CPU burst: %d\n",minCPUBurst);
    printf("Average CPU burst observed: %lf\n",(totalCPUBurst*1.0)/countCPUBurst);
    printf("Number of non-zero CPU burst: %d\n",countCPUBurst);
    printf("Average wait time: %lf\n",(totalWaitTime*1.0)/numThreads);
    printf("Maximum Completion Time: %d\n",maxCompletionTime);
    printf("Minimum Completion Time: %d\n",minCompletionTime);
    double avgCompletionTime = (totalCompletionTime*1.0)/noOfCompletedThreads;
    printf("Average Completion Time: %lf\n",avgCompletionTime);
   printf("Variance Completion Time: %lf\n",(totalSqCompletionTime*1.0)/noOfCompletedThreads - avgCompletionTime*avgCompletionTime); 
    if(scheduler->GetAlgo() == 2)
	printf("Error in estimate of cpu burst:%lf\n",(totalBurstError*1.0)/totalCPUBurst);
}
