// progtest.cc 
//	Test routines for demonstrating that Nachos can load
//	a user program and execute it.  
//
//	Also, routines for testing the Console hardware device.
//
// Copyright (c) 1992-1993 The Regents of the University of California.
// All rights reserved.  See copyright.h for copyright notice and limitation 
// of liability and disclaimer of warranty provisions.

#include "copyright.h"
#include "system.h"
#include "console.h"
#include "addrspace.h"
#include "synch.h"

void ForkStartUp(int file){
	currentThread->Startup();
	machine->Run();
}


//----------------------------------------------------------------------
// StartProcess
// 	Run a user program.  Open the executable, load it into
//	memory, and jump to it.
//----------------------------------------------------------------------

void
StartProcess(char *filename)
{
    OpenFile *executable = fileSystem->Open(filename);
    AddrSpace *space;

    if (executable == NULL) {
	printf("Unable to open file %s\n", filename);
	return;
    }
    space = new AddrSpace(executable);    
    currentThread->space = space;

    delete executable;			// close file

    space->InitRegisters();		// set the initial register values
    space->RestoreState();		// load page table register

    machine->Run();			// jump to the user progam
    ASSERT(FALSE);			// machine->Run never returns;
					// the address space exits
					// by doing the syscall "exit"
}

void ScheduleProcesses(char *file){ // Process -F option

	printf("Batch Processes are entering ready queue\n");
	
	FILE * schedInfo = fopen(file,"r");

	if(schedInfo == NULL){
		printf("Unable to open file %s\n",file);
		return;
	}

	char processName[100];
	char forkName[1024];
	int priority;
	int algo;
	int flag;
	int threadCount = 0;
	
	OpenFile *executable;

	fscanf(schedInfo,"%d\n",&algo);
	scheduler->SetAlgo(algo);

	while((flag = fscanf(schedInfo,"%s %d\n",processName,&priority))!=EOF){
		if(flag==1)
			priority = 100;
		executable = fileSystem->Open(processName);
		if(executable == NULL){
			printf("Unable to open file %s\n",processName);
			return;
		}
		sprintf(forkName,"%s_%d",processName+8,threadCount+1);
		NachOSThread * t = new NachOSThread(forkName);
		t->setBasePriority(50+priority);
		t->setPriority(50+priority);
		threadCount++;
		t->space  = new AddrSpace(executable);
		
		delete executable;
		t->space->InitRegisters();
		t->SaveUserState(); 		
	        t->setCreationTime(stats->totalTicks);		
		t->ThreadFork(ForkStartUp,0);
		//t->Schedule(); My mistake
	}

        // Put exit code 1 on register 2 and raise syscallexception for current thread that is main thre.
	stats->setNumThreads(threadCount + 1);

	machine->WriteRegister(2,1);
	machine->WriteRegister(4,0);
	ExceptionHandler(SyscallException);


}


// Data structures needed for the console test.  Threads making
// I/O requests wait on a Semaphore to delay until the I/O completes.

static Console *console;
static Semaphore *readAvail;
static Semaphore *writeDone;

//----------------------------------------------------------------------
// ConsoleInterruptHandlers
// 	Wake up the thread that requested the I/O.
//----------------------------------------------------------------------

static void ReadAvail(int arg) { readAvail->V(); }
static void WriteDone(int arg) { writeDone->V(); }

//----------------------------------------------------------------------
// ConsoleTest
// 	Test the console by echoing characters typed at the input onto
//	the output.  Stop when the user types a 'q'.
//----------------------------------------------------------------------

void 
ConsoleTest (char *in, char *out)
{
    char ch;

    console = new Console(in, out, ReadAvail, WriteDone, 0);
    readAvail = new Semaphore("read avail", 0);
    writeDone = new Semaphore("write done", 0);
    
    for (;;) {
	readAvail->P();		// wait for character to arrive
	ch = console->GetChar();
	console->PutChar(ch);	// echo it!
	writeDone->P() ;        // wait for write to finish
	if (ch == 'q') return;  // if q, quit
    }
}
